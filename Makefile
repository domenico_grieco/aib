all:
	mvn clean install -DskipTests

idea:
	mvn idea:idea

eclipse:
	mvn eclipse:eclipse

jar:
	mvn clean package -DskipTests
	
package: jar
	mkdir target/lib/main
	mv target/aib*.jar target/lib/main
	mkdir -p target/aib
	cp -r template/cfg target/aib
	cp -r template/bin target/aib
	cp -rp target/lib target/aib
	tar zcf target/aib.tgz target/aib
 	
	
	

