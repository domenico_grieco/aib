aib
===============
Below the steps to setup the environment:

====
DEVELOPMENT ENVIRONMENT SETUP

Compile and install NoSQL core library

1. Make the nosqlcore package into local maven repository

   $ cd <WORKSPACE>/AIB/nosql-core
   $ make local

3. Compile the prototypes

   $ cd <WORKSPACE>/AIB/aib
   $ mvn clean package

====
RUNTIME/TEST ENVIRONMENT SETUP

1. Install NoSQL library
     Copy the nosql-core-1.0.1_003.jar into $HBASE_HOME/lib on each data node and restart HBase from Ambari.

2. Install solr collections

    -  Move into directory containing the solr collections and run the command below
       $ cd aib/solr-collection
       $ solr create -c rules -d rules

    - restart solr

3. Deploy the test package 
 
     Into home directory decompress the AIB.tgz
     Configure the aib package according to configuration files into AIB/aib/cfg

5. Runt the test

    5.1) Load the XML rule configuration
     $ cd $HOME/AIB/aib/bin
     $ ./ruleLoader.sh <cfg-file> <rue-file>
     
    5.2) Run the spark streaming job
     $ cd $HOME/AIB/aib/bin
     $ ./start_aib_sim.sh

    5.3) Run the kafka job producer
     $ cd $HOME/AIB/aib/bin
     $ ./putKafka.sh
