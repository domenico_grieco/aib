import java.io.File

import com.gfmi.aib.parser.RuleXmlParser


object XmlTest extends App {
  val r = new RuleXmlParser()
  val res = r.parse(new File("/home/biadmin/dev/git/aib/src/main/resources/EVENT_RULES.xml"))
  res.foreach(println)

}
