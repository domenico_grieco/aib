import com.gfmi.aib.datasource.RuleSourceFactory
import com.typesafe.config.ConfigFactory

/**
  * Created by biadmin on 9/20/16.
  */
object SolrTest extends App {

  //val api = new SolrRuleApi("http://centos7-hadoop:8983/solr","rules")

  val conf = ConfigFactory.parseString(
    """
      |url:"http://centos7-hadoop:8983/solr"
      |collection:"rules"
      |
    """.stripMargin)

  val data = RuleSourceFactory.getSolrSource(conf.getString("url"), conf.getString("collection"))

  //data.fetchByHostname("onlinebanking.aib.ie").foreach(println)
  data.fetchByXpid().foreach(println)


}
