package com.gfmi.aib

/**
  * Created by biadmin on 9/20/16.
  */
object CustomTypos {
  type XmlConfiguration  = String
  type RuleFilter = (String,String)
}
