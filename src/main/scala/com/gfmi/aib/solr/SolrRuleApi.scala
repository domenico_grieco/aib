package com.gfmi.aib.solr


import com.gfmi.aib.solr.SolrRuleUtils._
import com.gfmi.aib.{HostnameRule, Rule, XpidRule}
import com.gfmi.solrindexer.{SolrIndexer, SolrQueryBuilder}
import com.google.common.collect.Lists

/**
  * Created by biadmin on 2/24/16.
  */
object SolrRuleApi {
  val HOSTNAME_RULE = "h"
  val XPID_RULE = "x"
}

class SolrRuleApi(solrUrl: String, collection: String) {


  val solr = new SolrIndexer(solrUrl, collection)

  def clearAll(): Unit = {
    val query = new SolrQueryBuilder(classOf[SolrRule])
    solr.deleteByQuery(query.toSolrQueryOnBaseResource)
  }

  def addOrUpdate(rule: Rule): Unit = {
    val list = Lists.newArrayList[SolrRule]()
    list.add(toSolrRule(rule))
    solr.uploadIstances(list)
  }

  def remove(rule: Rule): Unit = {
    solr.deleteById(classOf[SolrRule], toSolrRule(rule).id)
  }

  def findAll(): Seq[Rule] = {
    val query = new SolrQueryBuilder(classOf[SolrRule])
    solr.retrieveAllIstances(query.toSolrQueryOnBaseResource).toArray(Array[SolrRule]()).map(toRule)
  }

  def find(`type`: String = null, hostname: String = null, mt: String = null): Seq[Rule] = {
    val query = new SolrQueryBuilder(classOf[SolrRule])
    if (`type` != null)
      query.addConstraint(SolrRule.RULE_TYPE, `type`)
    if (hostname != null)
      query.addConstraint(SolrRule.RULE_HOSTNAME, hostname)
    solr.retrieveAllIstances(query.toSolrQueryOnBaseResource).toArray(Array[SolrRule]()).map(toRule)
  }

}


private[solr] object SolrRuleUtils {

  import SolrRuleApi._

  def toSolrRule(rule: Rule) = {
    rule match {
      case r: XpidRule =>
        new SolrRule(XPID_RULE, null, r.configuration)
      case r: HostnameRule =>
        new SolrRule(HOSTNAME_RULE, r.hostname, r.configuration)
      case u => throw new UnsupportedSolrClassException(u.getClass)
    }

  }


  def toRule(solrrule: SolrRule): Rule =
    solrrule.`type` match {
      case XPID_RULE => XpidRule(solrrule.conf)
      case HOSTNAME_RULE => HostnameRule(solrrule.hostname, solrrule.conf)
      case u => throw new UnsupportedSolrClassException(u.getClass)
    }


}

case class UnsupportedSolrClassException(c: Class[_]) extends RuntimeException(s"cannot support: ${c.getCanonicalName}")

case class UnsupportedSolrTypeClassException(c: String) extends RuntimeException(s"unsupported type: $c")