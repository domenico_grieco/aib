package com.gfmi.aib.config

import com.typesafe.config.Config

/**
  * Created by biadmin on 9/21/16.
  */

case class SparkStreamingConfig(
                                 sparkAppname: String,
                                 sparkMaster: String,
                                 sparkCheckpointdir: String,
                                 datasourceType: String,
                                 solrUrl: String,
                                 solrCollection: String,
                                 hbaseHbasesite: String,
                                 kafkaTopics: String,
                                 kafkaBrokerslist: String,
                                 kafkaGroupid: String
                               )

object SparkStreamingConfig {

  def create(config: Config) = {
    SparkStreamingConfig(
      config.getString("spark.appname"),
      config.getString("spark.master"),
      config.getString("spark.checkpointdir"),
      config.getString("datasource.type"),
      config.getString("datasource.conf.url"),
      config.getString("datasource.conf.collection"),
      config.getString("datasource.conf.hbasesite"),
      config.getString("kafka.topics"),
      config.getString("kafka.brokerslist"),
      config.getString("kafka.groupid"))
  }
}
