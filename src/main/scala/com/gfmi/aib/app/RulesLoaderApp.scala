package com.gfmi.aib.app

import java.io.File

import com.gfmi.aib.datasource.RuleSourceFactory
import com.gfmi.aib.parser.RuleXmlParser
import com.typesafe.config.ConfigFactory
import org.apache.log4j.Logger

/**
  * Created by biadmin on 9/20/16.
  *
  * arg0 = configuration file
  * arg1 =
  */
object RulesLoaderApp extends App {

  private val log = Logger.getLogger(getClass)

  val Array(configFile, ruleFile) = args
  val conf = ConfigFactory.parseFile(new File(configFile))
  println(conf)

  val datasourceType = conf.getString("datasource.type")
  val datasourceConf = conf.getConfig("datasource.conf")
  val rulesDB = RuleSourceFactory.get(datasourceType, datasourceConf)

  val parser = new RuleXmlParser()

  val xmlrules = parser.parse(new File(ruleFile))
  rulesDB.saveInBatch(xmlrules)

  log.info("Terminated")

}
