package com.gfmi.aib.app

import java.io.File
import java.util.Properties

import com.typesafe.config.ConfigFactory
import kafka.producer.{KeyedMessage, Producer, ProducerConfig}
import org.apache.log4j.Logger

/**
  * Created by biadmin on 9/22/16.
  */
object PutKafkaApp extends App {

  val log = Logger.getLogger(getClass)
  val json = "{\"vid\":\"1549299243591562\",\"timestamp\":1473645604945,\"deviceType\":\"Android\",\"os\":\"Android\",\"browser\":\"Chrome\",\"platform\":\"Linux armv8l\",\"ua\":\"Mozilla/5.0 (Linux; Android 6.0.1; SM-G920F Build/MMB29K) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.98 Mobile Safari/537.36\",\"href\":\"https://personal.aib.ie/personal-forms/forgotten-registration\",\"pageLoad\":\"1\",\"pageTitle\":\"Internet Banking Login Reset\",\"referrer\":\"https://onlinebanking.aib.ie/inet/roi/login.htm\",\"logged_in\":0,\"pageHeader\":\"Internet Banking Login Reset\",\"pageHeader2\":\"\",\"pageHeader3\":\"Personal Details\",\"maxPageHeader\":\"\",\"maxPageHeader2\":\"\",\"maxPageHeader3\":\"Security Check\",\"rulesPublishedDate\":\"2016-02-26 09:58:12 UTC\",\"hostname\":\"personal.aib.ie\",\"pathname\":\"/personal-forms/forgotten-registration\",\"Event_Type\":\"User input\",\"Event_Name\":\"card_name\",\"Event_Value\":\"Mr Dean Xyz\",\"requestIP\":\"192.78.165.29\",\"arrivalTime\":1473645606925}"

  val conf = ConfigFactory.parseFile(new File(args(0)))
  val brokerList = conf.getString("kafka.brokerslist")
  val topic = conf.getString("kafka.topic")
  val timeout = conf.getInt("app.timeout")
  val nReq = conf.getInt("app.numreq")

  val props = new Properties()
  props.put("metadata.broker.list", brokerList)
  props.put("serializer.class", "kafka.serializer.StringEncoder")
  props.put("producer.type", "async")

  val config = new ProducerConfig(props)
  val producer = new Producer[String, String](config)
  while (true) {

    log.info(s"sending $nReq requests...")
    (1 until nReq).foreach { i =>
      val data = new KeyedMessage[String, String](topic, json)
      producer.send(data)
    }
    log.info(s"sleep for $timeout milliseconds ...")
    Thread.sleep(timeout)
  }

  producer.close()

}
