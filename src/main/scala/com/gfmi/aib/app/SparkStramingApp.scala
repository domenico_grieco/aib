package com.gfmi.aib.app

import com.gfmi.aib.config.SparkStreamingConfig
import com.gfmi.aib.datasource.RuleSourceFactory
import com.typesafe.config.ConfigFactory
import kafka.serializer.StringDecoder
import org.apache.log4j.Logger
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka.{HasOffsetRanges, KafkaUtils, OffsetRange}
import org.apache.spark.streaming.{Seconds, StreamingContext}

import scala.util.{Failure, Success, Try}

/**
  * Created by biadmin on 9/20/16.
  */
object SparkStramingApp {
  val log = Logger.getLogger(getClass)

  def main(args: Array[String]): Unit = {

    // confFile = "../Spark-Streaming-Test/src/main/resources/spark-streming.conf"

    val conf = SparkStreamingConfig.create(ConfigFactory.parseString(args(0)))

    log.info(s""" $conf """)

    val ssc = StreamingContext.getOrCreate(conf.sparkCheckpointdir, () => functionToCreateContext(conf))

    ssc.start()
    ssc.awaitTermination()

  }


  def functionToCreateContext(configuration: SparkStreamingConfig): StreamingContext = {

    val conf = new SparkConf().setAppName(configuration.sparkAppname).setMaster(configuration.sparkMaster)

    val ssc = new StreamingContext(conf, Seconds(5))

    val topicsSet = configuration.kafkaTopics.split(",").toSet
    val kafkaParams = Map[String, String]("metadata.broker.list" -> configuration.kafkaBrokerslist,
      "group.id" -> configuration.kafkaGroupid,
      "auto.offset.reset" -> "smallest")

    val messages = KafkaUtils.createDirectStream[String, String, StringDecoder, StringDecoder](ssc, kafkaParams, topicsSet)

    var offsetRanges = Array[OffsetRange]()
    // parse blocks
    val blocks = messages.transform { rdd =>
      offsetRanges = rdd.asInstanceOf[HasOffsetRanges].offsetRanges
      rdd.map(rdd => {
        val c = ConfigFactory.parseString(rdd._2)
        (c.getString("vid"), c)
      })
    }
      .foreachRDD(rdd => {
        rdd.foreachPartition(it => {
          val rulesDB = RuleSourceFactory.get(configuration.datasourceType, configuration)

          while (it.hasNext) {
            val next = it.next()

            println(s"VID : ${next._1} => JSON :${next._2}")
            Try(next._2.getString("hostname")) match {
              case Success(host) =>
                val rule = rulesDB.fetchByHostname(host)
                println(s"HOSTNAME : $host => RULE :$rule")
              case Failure(f) =>
                log.warn("No hostname key in json")
                Try(next._2.getString("xpid")) match {
                  case Success(xpid) =>
                    val rule = rulesDB.fetchByXpid()
                    println(s"HOSTNAME : $xpid => RULE :$rule")
                  case Failure(_) => log.warn("No xpid key in json")
                }
            }
          }
          rulesDB.closeConnection()
        })
      })

    ssc
  }

}
