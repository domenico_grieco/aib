package com.gfmi.aib

import com.gfmi.aib.CustomTypos.XmlConfiguration

/**
  * Created by biadmin on 9/20/16.
  */
trait Rule {
  def configuration():XmlConfiguration
}

case class HostnameRule(hostname:String, configuration:XmlConfiguration) extends Rule()
case class XpidRule(configuration:XmlConfiguration) extends Rule()
