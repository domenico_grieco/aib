package com.gfmi.aib.parser

import java.io.File

import com.gfmi.aib.{HostnameRule, Rule, XpidRule}

import scala.xml.{NodeSeq, XML}

/**
  * Created by biadmin on 9/20/16.
  */
class RuleXmlParser {

  def parse(file: File): Iterable[Rule] = {
    val xml = XML.loadFile(file)

    //  scala.xml.Elem

    val set = xml \\ "eventRules" \ "ruleSet"
    val groupslist = set.map(s => {
      val gl = s \ "ruleGroupList"
      val c = s \ "comparison"
      GroupList(gl, parseComparison(c))
    }
    )



    val rules  = groupslist.flatMap {
      glist =>
        glist.comparison.key match {
          case "hostname" =>
            val xml = glist.grouplist toString()
            glist.comparison.values.map(v => HostnameRule(v, xml))

          case "xpid" =>
            val xml = glist.grouplist toString()
            Seq(XpidRule(xml))
          case _ => throw new IllegalArgumentException()
        }


    }

    rules

  }


  def parseComparison(e: NodeSeq): Comparison = {
    val `type` = e \ "@type" text
    val key = e \ "key" text
    val values = e \ "valueList" \\ "value" map (_ text)

    Comparison(key, values, `type`)
  }

}


case class GroupList(grouplist: NodeSeq, comparison: Comparison)

case class Comparison(key: String, values: Seq[String], `type`: String)
