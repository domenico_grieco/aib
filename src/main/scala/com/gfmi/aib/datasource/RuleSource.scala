package com.gfmi.aib.datasource

import com.gfmi.aib.CustomTypos.XmlConfiguration
import com.gfmi.aib.{HostnameRule, Rule, XpidRule}

/**
  * Created by biadmin on 9/20/16.
  */
trait RuleSource {


  def saveInBatch(rules: Iterable[Rule]) = rules.foreach(save)

  def save(rule: Rule) {
    rule match {
      case r: HostnameRule => saveHostnameRule(r)
      case r: XpidRule => saveXpidRule(r)
      case u => throw new IllegalRuleException(u)
    }
  }

  def fetchByHostname(hostname: String): Option[XmlConfiguration]

  def fetchByXpid(): Option[XmlConfiguration]

  def saveHostnameRule(rule: HostnameRule)

  def saveXpidRule(rule: XpidRule)

  def closeConnection()
}


case class IllegalRuleException(msg: Any) extends RuntimeException(if (msg != null) msg.toString else null)