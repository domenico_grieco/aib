package com.gfmi.aib.datasource

import java.net.URL

import com.gfmi.aib.CustomTypos.XmlConfiguration
import com.gfmi.aib.bigtable.dao.RuleDAO
import com.gfmi.aib.bigtable.dao.RuleDAO._
import com.gfmi.aib.bigtable.entity.RuleEntry
import com.gfmi.aib.{HostnameRule, XpidRule}
import com.ti.oss.nosql.core.{BTCompOp, BTConstraints}
import com.ti.oss.nosql.utils.Utils
import org.apache.log4j.Logger

/**
  * Created by biadmin on 9/20/16.
  */
class HbaseRuleSource(hbaseSite: String) extends RuleSource {

  val EMPTY_XPID = ""

  Utils.setHbaseSite(new URL(hbaseSite))

  val log = Logger.getLogger(getClass)
  val ruleDAO = getDAOInstance

  override def fetchByHostname(hostname: String): Option[XmlConfiguration] = {
    val it = ruleDAO.findByPropertiesInAndIterator(BTConstraints.create()
      .and(BTCompOp.EQUAL, PREFIX, H_PREFIX)
      .and(BTCompOp.EQUAL, RESOURCE, hostname))

    if (it.hasNext)
      Some(it.next().getValue())
    else
      None
  }

  override def saveXpidRule(rule: XpidRule): Unit = {
    log.info(s"Save entry in RULE table : $rule")
    ruleDAO.save(new RuleEntry(X_PREFIX, EMPTY_XPID, rule.configuration))
  }

  override def saveHostnameRule(rule: HostnameRule): Unit = {
    log.info(s"Save entry in RULE table : $rule")
    ruleDAO.save(new RuleEntry(H_PREFIX, rule.hostname, rule.configuration))
  }

  override def fetchByXpid(): Option[XmlConfiguration] = {
    val it = ruleDAO.findByPropertiesInAndIterator(BTConstraints.create()
      .and(BTCompOp.EQUAL, PREFIX, X_PREFIX)
    )

    if (it.hasNext)
      Some(it.next().getValue())
    else
      None
  }

  override def closeConnection(): Unit = ???

  private def getDAOInstance: RuleDAO = {
    val ruleDAO = RuleDAO()
    if (!ruleDAO.existsTable()) {
      ruleDAO.createTable()
      log.info(ruleDAO.getTableName + " created")
    }
    ruleDAO
  }
}
