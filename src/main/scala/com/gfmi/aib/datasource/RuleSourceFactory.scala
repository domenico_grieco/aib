package com.gfmi.aib.datasource

import com.gfmi.aib.config.SparkStreamingConfig
import com.typesafe.config.Config

/**
  * Created by biadmin on 9/20/16.
  */
object RuleSourceFactory {

  def get(datasourceType: String, config: Config): RuleSource = {
    datasourceType match {
      case "solr" => new SolrRuleSource(config.getString("url"), config.getString("collection"))
      case "hbase" => new HbaseRuleSource(config.getString("hbasesite"))
      case u => throw IllegalDataSourceException(u)
    }
  }

  def get(datasourceType: String, config: SparkStreamingConfig): RuleSource = {
    datasourceType match {
      case "hbase" => hbase(config)
      case "solr" => solr(config)
      case u => throw IllegalDataSourceException(u)
    }
  }

  def solr(config: SparkStreamingConfig) = new SolrRuleSource(config.solrUrl, config.solrCollection)

  def hbase(config: SparkStreamingConfig) = new HbaseRuleSource(config.hbaseHbasesite)

  def getHbaseSource(hbasesite: String) = new HbaseRuleSource(hbasesite)

  def getSolrSource(solrUrl: String, solrCollection: String) = new SolrRuleSource(solrUrl, solrCollection)

}

case class IllegalDataSourceException(msg: String) extends RuntimeException(if (msg != null) msg.toString else null)
