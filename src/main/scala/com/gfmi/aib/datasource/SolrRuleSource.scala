package com.gfmi.aib.datasource

import com.gfmi.aib.CustomTypos.XmlConfiguration
import com.gfmi.aib.solr.SolrRuleApi
import com.gfmi.aib.{HostnameRule, XpidRule}
import org.apache.log4j.Logger

/**
  * Created by biadmin on 9/20/16.
  */
class SolrRuleSource(solrUrl: String, solrCollection: String) extends RuleSource {

  private val log = Logger.getLogger(getClass)

  log.info(s"SOLR ULR        :$solrUrl")
  log.info(s"SOLR COLLECTION :$solrCollection")

  val solr = new SolrRuleApi(solrUrl, solrCollection)

  override def fetchByHostname(hostname: String): Option[XmlConfiguration] = {
    val res = solr.find(hostname = hostname, `type` = SolrRuleApi.HOSTNAME_RULE)
    if (res.isEmpty)
      None
    else
      Some(res.head.configuration())
  }

  override def saveXpidRule(rule: XpidRule): Unit = {
    solr.addOrUpdate(rule)
  }

  override def saveHostnameRule(rule: HostnameRule): Unit = {
    solr.addOrUpdate(rule)
  }

  override def fetchByXpid(): Option[XmlConfiguration] = {
    val res = solr.find(`type` = SolrRuleApi.XPID_RULE)
    if (res.isEmpty)
      None
    else
      Some(res.head.configuration())
  }

  override def closeConnection() = {
    log.info("Closing connection...")
    solr.solr.getSolrClient.shutdown()
  }
}
