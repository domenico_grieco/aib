package com.gfmi.aib.bigtable.entity

import com.gfmi.aib.bigtable.dao.RuleDAO._
import com.ti.oss.nosql.core.model.{AbstractEntity, Key}

/**
  * Created by biadmin on 9/20/16.
  */
class RuleEntry extends AbstractEntity {

  val prefix = getColumnValue(PREFIX, classOf[String])
  val resource = getColumnValue(RESOURCE, classOf[String])


  def this(prefix: String, resource: String, value: String) = {
    this()
    setColumnValue(PREFIX, prefix)
    setColumnValue(RESOURCE, resource)
    setColumnValue(CF, CQ, value)
  }

  def getPrefix(): String = {
    getColumnValue(PREFIX, classOf[String])
  }

  def getResource(): String = {
    getColumnValue(RESOURCE, classOf[String])
  }

  def getValue(): String = {
    getColumnValue(CF, CQ, classOf[String])
  }

  override def getKey: Key = Key.create()
    .add(PREFIX, getColumnValue(PREFIX))
    .add(RESOURCE, getColumnValue(RESOURCE))

}
