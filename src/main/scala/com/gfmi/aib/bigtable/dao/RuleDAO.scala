package com.gfmi.aib.bigtable.dao

import com.gfmi.aib.bigtable.entity.RuleEntry
import com.ti.oss.nosql.core.BTColumnInfo
import com.ti.oss.nosql.core.model.dao.BaseDAO


/**
  * Created by biadmin on 9/20/16.
  */
object RuleDAO {
  val H_PREFIX = "H"
  val X_PREFIX = "X"
  val PREFIX = "P"
  val RESOURCE = "R"
  val CF = "CF"
  val CQ = "CQ"
  val tableName = "RULE"

  val rowKeys = Array(
    new BTColumnInfo(PREFIX, classOf[String]),
    new BTColumnInfo(RESOURCE, classOf[String]))
  val families = Array(CF)

  def apply(): RuleDAO = {
    val dao = new RuleDAO(tableName, rowKeys, families)
    if (!dao.existsTable())
      dao.createTable()
    dao
  }

}

class RuleDAO(tableName: String, rowKeys: Array[BTColumnInfo], families: Array[String]) extends BaseDAO[RuleEntry](tableName, rowKeys, families, classOf[RuleEntry]) {
}
