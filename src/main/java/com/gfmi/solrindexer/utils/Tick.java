package com.gfmi.solrindexer.utils;

import java.util.Date;

/**
 * Created by fre on 01/10/15.
 * Class used to take time of operations
 * Just create the istance and than call getTock from it;
 */
public class Tick {
    public final long tick;

	public Tick() {
        tick = new Date().getTime();
    }

	public long getTock() {
        return new Date().getTime() - tick;
    }

	public double getTockInSeconds(){return ((float)getTock())/1000;}
}
