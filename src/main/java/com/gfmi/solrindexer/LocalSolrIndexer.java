package com.gfmi.solrindexer;

import org.apache.solr.client.solrj.embedded.EmbeddedSolrServer;
import org.apache.solr.core.CoreContainer;

import java.io.File;

/**
 * Created by fre on 30/11/15.
 * Solr indexer using embedded solr, useful for testing purpose
 */
public class LocalSolrIndexer extends SolrIndexer {

    private final File collectionFolder;
    private final File dataFolder;
    /**
     *
     * @param solrBasePath
     * @param collectionName
     * @param deleteExistingData should eventually presents data indexed be deleted or not?
     */
    public LocalSolrIndexer(final String solrBasePath, final String collectionName, boolean deleteExistingData) {
        super();
        this.collectionFolder = new File(solrBasePath+ File.separator+collectionName);
        //Delete the data folder just to be sure that each test is not affected from others
        this.dataFolder = new File(solrBasePath+File.separator+ collectionName +File.separator+"data");
        if (dataFolder.isDirectory() && deleteExistingData) dataFolder.delete();
        CoreContainer container = new CoreContainer(solrBasePath);
        container.load();
        solrClient = new EmbeddedSolrServer(container,collectionName);
    }

    public LocalSolrIndexer(final String solrBasePath, final String collectionName) {
        this(solrBasePath,collectionName,false);
    }

    public void shutdown(){
        getSolrClient().shutdown();
    }
}
