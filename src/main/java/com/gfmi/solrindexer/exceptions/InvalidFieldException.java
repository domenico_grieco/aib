package com.gfmi.solrindexer.exceptions;

/**
 * Created by fre on 22/09/15.
 */
public class InvalidFieldException extends Exception{
    public InvalidFieldException(String fieldName, Class aClass){
        super(String.format("Invalid field %s for class %s", fieldName, aClass.getCanonicalName()));
    }

    public InvalidFieldException(String message){
        super(message);
    }
}
