package com.gfmi.solrindexer.exceptions;

/**
 * Exception throwed when something goes wrong on loading solr configurations
 */
public class SolrConfigurationException extends Throwable {

}
