package com.gfmi.solrindexer.exceptions;

/**
 * Created by fre on 19/03/15.
 */
public class ClassNotSpecifiedException extends Exception {
    public ClassNotSpecifiedException(){
        super("Cannot analyze a document whose class is not specified");
    }
}
