package com.gfmi.solrindexer.exceptions;

import java.lang.reflect.Field;

/**
 * Created by fre on 14/10/15.
 */
public class TransformationException extends Exception {
    public TransformationException(String message){
        super(message);
    }

    public TransformationException(Object istance,Field field){
        super(String.format("Error when transforming from/to field %s for istance %s",field.getName(),istance.toString()));
    }

}
