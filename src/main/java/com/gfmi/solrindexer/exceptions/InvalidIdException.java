package com.gfmi.solrindexer.exceptions;

/**
 * Created by fre on 22/09/15.
 */
public class InvalidIdException extends Exception {
    public InvalidIdException(final String id){
        super(String.format("Cannot parse or use the id %s, invalid id",id));
    }
}
