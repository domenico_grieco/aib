package com.gfmi.solrindexer.exceptions;

import org.apache.solr.common.SolrDocument;

/**
 * Created by fre on 21/09/15.
 */
public class InvalidClassException extends Exception{
    public InvalidClassException(String classNameWeWishToConvert, SolrDocument document){
        super(String.format("Tried to convert to class %s the following solr document : %s", classNameWeWishToConvert,document.toString()));
    }
}
