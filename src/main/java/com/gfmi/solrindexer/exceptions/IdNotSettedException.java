package com.gfmi.solrindexer.exceptions;

/**
 * Created by fre on 18/03/15.
 */
public class IdNotSettedException extends Exception {
    public IdNotSettedException(){
        super("Cannot save an instance with no IdentiFier setted");
    }
}
