package com.gfmi.solrindexer.exceptions;

import com.gfmi.solrindexer.models.BaseSolrIndexableResource;

/**
 * Created by fre on 24/09/15.
 */
public class InvalidBaseResourceException extends Exception {
    public InvalidBaseResourceException(Class<? extends BaseSolrIndexableResource> aClass, String fieldName){
        super(String.format("Exception creating the class %s, the field name %s contains forbidden character that will be used in solr for Dynamic fields usage",aClass.getCanonicalName(),fieldName));
    }
}
