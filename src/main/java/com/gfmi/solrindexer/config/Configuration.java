package com.gfmi.solrindexer.config;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

/**
 * Class that containts the conf property obj and keys.
 * This class will load the file properties and expose the map of
 * the properties
 */
public class Configuration {
    static Configuration configuration;

    public static Configuration getConfiguration(){
        if (configuration == null){
            configuration = new Configuration();
        }
        return configuration;
    }

    /**
     * Force to reload the configuration.
     */
    public static void clearConfiguration(){
        configuration = null;
    }

    /**
     * Non singleton constructor used just for testing
     * @return
     */
    static Configuration getConfigurationForTest(){
        return new Configuration();
    }

    static Logger logger = Logger.getLogger(Configuration.class);
    //Key fetched from the JVM
    public final static String CONFIGURATION_PROPERTY_PATH_KEY = "solrindexer.configuration";

    //**Property keys**//
    //Keys for the property file about fields
    private final static String FIELD_POSTFIX_SEPARATOR_KEY = "field.postfix.separator";
    private final static String STRING_POSTFIX_KEY = "string.postfix";
    private final static String INTEGER_POSTFIX_KEY = "integer.postfix";
    private final static String FLOAT_POSTFIX_KEY = "float.postfix";
    private final static String LONG_POSTFIX_KEY = "long.postfix";
    private final static String BOOLEAN_POSTFIX_KEY = "boolean.postfix";
    private final static String DOUBLE_POSTFIX_KEY = "double.postfix";
    private final static String DATE_POSTFIX_KEY = "date.postfix";
    //Multivalue postfix
    private final static String MULTIVALUE_POSTFIX_KEY  = "multivalue.postfix";
    //Id separator
    private final static String ID_FIELD_SEPARATOR_KEY = "id.field.separator";
    //**End property keys**//

    //**Property values**//
    public final String FIELD_POSTFIX_SEPARATOR;
    public final String STRING_POSTFIX;
    public final String INTEGER_POSTFIX;
    public final String FLOAT_POSTFIX;
    public final String LONG_POSTFIX;
    public final String BOOLEAN_POSTFIX;
    public final String DOUBLE_POSTFIX;
    public final String DATE_POSTFIX;
    public final String MULTIVALUE_POSTFIX;
    public final String ID_FIELD_SEPARATOR;
    public final String CLASSNAME_SOLR_FIELD;
    //**End property values**//

    //Class - Related postfix map;
    public final Map<Class,String> CLASS_FIELD_MAP;

    //Constant properties
    public final String ID_FIELD = "id";
    public final String CLASSNAME_FIELD_NAME="className";

    private Map<String,String> properties;

    private Configuration() {
        Map<String,String> defaultProperties = getDefaultConfiguration();
        //Load default properties
        String propertyPath = System.getProperty(CONFIGURATION_PROPERTY_PATH_KEY);
        if (propertyPath != null && !propertyPath.isEmpty()){
            try {
                Properties userProperties = new Properties();
                userProperties.load(new InputStreamReader(new FileInputStream(propertyPath)));
                //Update properties overriding the defaults with the used defined ones
                logger.info("Updating default property file with file in " + propertyPath);
                for (Map.Entry<String,String> entry : getMapFromProp(userProperties).entrySet())
                    defaultProperties.put(entry.getKey(),entry.getValue());
            }catch (IOException e){
                logger.error("Error trying to load the configuration file from the system property setted path " + propertyPath);
            }
        } else{
            logger.info("Default property loaded");
        }
        this.properties = defaultProperties;

        //Filling the field now
        FIELD_POSTFIX_SEPARATOR = properties.get(FIELD_POSTFIX_SEPARATOR_KEY);
        STRING_POSTFIX = properties.get(STRING_POSTFIX_KEY);
        INTEGER_POSTFIX = properties.get(INTEGER_POSTFIX_KEY);
        FLOAT_POSTFIX = properties.get(FLOAT_POSTFIX_KEY);
        LONG_POSTFIX = properties.get(LONG_POSTFIX_KEY);
        BOOLEAN_POSTFIX = properties.get(BOOLEAN_POSTFIX_KEY);
        DOUBLE_POSTFIX = properties.get(DOUBLE_POSTFIX_KEY);
        DATE_POSTFIX = properties.get(DATE_POSTFIX_KEY);
        MULTIVALUE_POSTFIX = properties.get(MULTIVALUE_POSTFIX_KEY);
        ID_FIELD_SEPARATOR = properties.get(ID_FIELD_SEPARATOR_KEY);
        CLASSNAME_SOLR_FIELD = CLASSNAME_FIELD_NAME+FIELD_POSTFIX_SEPARATOR+STRING_POSTFIX;

        CLASS_FIELD_MAP = ImmutableMap.<Class, String>builder()
                .put(String.class, FIELD_POSTFIX_SEPARATOR + STRING_POSTFIX)
                .put(Integer.class, FIELD_POSTFIX_SEPARATOR + INTEGER_POSTFIX)
                .put(Float.class, FIELD_POSTFIX_SEPARATOR + FLOAT_POSTFIX)
                .put(Long.class, FIELD_POSTFIX_SEPARATOR + LONG_POSTFIX)
                .put(Boolean.class, FIELD_POSTFIX_SEPARATOR + BOOLEAN_POSTFIX)
                .put(Double.class, FIELD_POSTFIX_SEPARATOR + DOUBLE_POSTFIX)
                .put(Date.class, FIELD_POSTFIX_SEPARATOR + DATE_POSTFIX)
                .build();
    }

    private Map<String,String> getDefaultConfiguration() {
        Map<String,String> m = new HashMap<>(10);
        //The values should be putted in constant variables...we can survive to this.
        m.put(FIELD_POSTFIX_SEPARATOR_KEY,"_");
        m.put(STRING_POSTFIX_KEY,"s");
        m.put(INTEGER_POSTFIX_KEY,"i");
        m.put(FLOAT_POSTFIX_KEY,"f");
        m.put(LONG_POSTFIX_KEY,"l");
        m.put(BOOLEAN_POSTFIX_KEY,"b");
        m.put(DOUBLE_POSTFIX_KEY,"d");
        m.put(DATE_POSTFIX_KEY,"dt");
        m.put(MULTIVALUE_POSTFIX_KEY,"s");
        m.put(ID_FIELD_SEPARATOR_KEY,"|");
        return m;
    }

    ImmutableSet<String> requiredKeysSet = ImmutableSet.of(
            ID_FIELD_SEPARATOR_KEY,
            MULTIVALUE_POSTFIX_KEY,
            DATE_POSTFIX_KEY,
            DOUBLE_POSTFIX_KEY,
            BOOLEAN_POSTFIX_KEY,
            LONG_POSTFIX_KEY,
            FLOAT_POSTFIX_KEY,
            INTEGER_POSTFIX_KEY,
            STRING_POSTFIX_KEY,
            FIELD_POSTFIX_SEPARATOR_KEY
    );


    public void printConfiguration(){
        for (Map.Entry<String,String> entry : properties.entrySet()){
            System.out.println(String.format("%s -> %s", entry.getKey(), entry.getValue()));
        }
    }

    Map<String,String> getMapFromProp(Properties properties)  {
        Map<String,String> conf = new LinkedHashMap<>();
        for (String key : requiredKeysSet){
            if (properties.getProperty(key) != null && !properties.getProperty(key).isEmpty())
                conf.put(key, properties.getProperty(key));
            else
                logger.warn("In conf file null or empty value for key "+ key);
        }
        return conf;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Configuration that = (Configuration) o;

        return !(properties != null ? !properties.equals(that.properties) : that.properties != null);

    }

    @Override
    public int hashCode() {
        return properties != null ? properties.hashCode() : 0;
    }
}
