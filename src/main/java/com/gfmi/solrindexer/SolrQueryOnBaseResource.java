package com.gfmi.solrindexer;

import org.apache.solr.client.solrj.SolrQuery;

/**
 * Just an extension of a SolrQuery, to make us sure the part of the query has been built using
 * our SolrQueryBuilder
 */
public class SolrQueryOnBaseResource extends SolrQuery {
    final SolrQueryBuilder solrQueryBuilder;
    public SolrQueryOnBaseResource(SolrQueryBuilder solrQueryBuilder){
        super(solrQueryBuilder.getStringQuery());
        this.solrQueryBuilder = solrQueryBuilder;
    }
}
