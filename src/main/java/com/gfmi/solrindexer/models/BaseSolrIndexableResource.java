package com.gfmi.solrindexer.models;

import com.gfmi.solrindexer.config.Configuration;
import com.gfmi.solrindexer.ClassUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by fre on 21/09/15.
 */
public class BaseSolrIndexableResource implements Serializable{
    public String id;
    public String className;

    public BaseSolrIndexableResource(){}

    public BaseSolrIndexableResource(final String id) {
        this.id = id;
        this.className = this.getClass().getCanonicalName();
    }

    @Override
    public String toString() {
        return this.getClass().getCanonicalName()+"{" +
                "id='" + id + '\'' +
                ", className='" + className + '\'' +
                '}';
    }

    /**
     * Method used to check if the class you are creating is valid.
     * Should be performed on each class extending BaseSolrIndexableResource at the beginning of the program.
     * Using invalid class definition could take to a wrong/unexpected behaviour
     * Class is valid if all fields name do not contain the FIELD_POSTFIX_SEPARATOR value
     * and all the fields have a unique name.
     * @return true if the class definition is valid, false otherwise.
     */
    public boolean isValidClassDefinition() {
        Set<String> fieldsNameSet = new LinkedHashSet<>();
        for (Map.Entry<Field,String> entry : ClassUtils.getAllSerializableFields(this.getClass()).entrySet()) {
            if (entry.getValue().contains(Configuration.getConfiguration().FIELD_POSTFIX_SEPARATOR))
                return false;
            if (fieldsNameSet.contains(entry.getValue()))
                return false;
            else
                fieldsNameSet.add(entry.getValue());
        }
        return true;
    }

    /**
     * Check if an istance is valid.
     * @return true if is valid, false otherwise
     */
    public boolean isValidIstance(){
        return (
                (id != null)
                && (!id.trim().isEmpty())
                && (className != null)
                && (!className.trim().isEmpty())
                && (!id.contains(Configuration.getConfiguration().ID_FIELD_SEPARATOR))
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseSolrIndexableResource that = (BaseSolrIndexableResource) o;

        return id.equals(that.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

