package com.gfmi.solrindexer.models;

import com.gfmi.solrindexer.interfaces.SolrSpecialField;
import com.gfmi.solrindexer.exceptions.TransformationException;

import java.io.Serializable;

/**
 * Created by fre on 14/10/15.
 * Class that in solr will be mapped to a field with type location.
 * Accordingly to the schema.xml and/or the Solr version this type can be mapped to a
 * class "solr.LatLonType" or a class "solr.SpatialRecursivePrefixTreeFieldType" (Which is faster and newer)
 */
public class Geopoint implements Serializable,SolrSpecialField {
    public static String GEOPOINT_POSTFIX = "_g";
    public double lat;
    public double lng;


    public Geopoint(){}

    public Geopoint(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Geopoint geopoint = (Geopoint) o;

        if (Double.compare(geopoint.lat, lat) != 0) return false;
        return Double.compare(geopoint.lng, lng) == 0;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(lat);
        result = (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lng);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public Object getSolrFieldValue() {
        return lat+" "+lng;
    }

    @Override
    public SolrSpecialField fillFromSolrFieldValue(Object valueThatComesFromSolr) throws TransformationException {
        String valueFromSolr = (String) valueThatComesFromSolr;
        try {

            String[] s = valueFromSolr.split(" ");
            Double lat = Double.valueOf(s[0]);
            Double lng = Double.valueOf(s[1]);
            return new Geopoint(lat,lng);
        }catch (NumberFormatException e){
            throw new TransformationException(e.getMessage());
        }
    }

    @Override
    public String getDynamicFieldPostfix() {
        return GEOPOINT_POSTFIX;
    }


    @Override
    public String toString() {
        return "Geopoint{" +
                "lat=" + lat +
                ", lng=" + lng +
                '}';
    }
}
