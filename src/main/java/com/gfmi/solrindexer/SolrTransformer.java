package com.gfmi.solrindexer;

import com.gfmi.solrindexer.config.Configuration;
import com.gfmi.solrindexer.exceptions.ClassNotSpecifiedException;
import com.gfmi.solrindexer.exceptions.TransformationException;
import com.gfmi.solrindexer.interfaces.SolrSpecialField;
import com.gfmi.solrindexer.models.BaseSolrIndexableResource;
import com.google.common.base.Optional;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;
import org.apache.log4j.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.*;


/**
 * Created by fre on 18/03/15.
 */
public class SolrTransformer {

    static Logger logger = Logger.getLogger(SolrTransformer.class);
    private static final String VERSION_SOLR_FIELD = "_version_";


    /**
     * Given a solrFieldName return the actual fieldName
     * @param solrFieldName
     * @return
     */
    static String getActualFieldNameFromSolrFieldName(final String solrFieldName){
        return solrFieldName.substring(0, solrFieldName.lastIndexOf(Configuration.getConfiguration().FIELD_POSTFIX_SEPARATOR));
    }


    /**
     * Check if is a valid SolrDocument that could be converted in an actual class
     * @param document
     * @return
     */
    static boolean isValidDocument(SolrInputDocument document){
        return !document.isEmpty() &&
                document.containsKey(Configuration.getConfiguration().ID_FIELD) &&
                document.containsKey(Configuration.getConfiguration().CLASSNAME_SOLR_FIELD);
    }

    /**
     * @param <T>
     * @param istance
     * @throws IllegalAccessException if tries to access to fields that are not public, this should never happen,
     * all fields needs to be public in order to produce cleaner code
     */
    static <T extends BaseSolrIndexableResource> Optional<SolrInputDocument> toSolrDocument(T istance){
        if (!istance.isValidIstance()){
            logger.error("Cannot save the istance " + istance.toString() + " istance is not valid");
            return Optional.absent();
        }
        SolrInputDocument document = new SolrInputDocument();
        //Attribute conversion
        try {
            //Will throw exception if no empty constructor are found
            Class clazz = istance.getClass();

            for(Map.Entry<Field,String> entry : ClassUtils.getAllSerializableFields(clazz).entrySet()){
                Field fieldToSave = entry.getKey();
                Object fieldToSaveValue = fieldToSave.get(istance);
                Optional<String> solrFieldName = ClassUtils.getFieldNameInSolr(fieldToSave);
                if (fieldToSaveValue == null){
                    logger.debug("Null value on field " + fieldToSave.getName() + ", ignoring it on istance " + istance);
                    continue;
                }
                if (!solrFieldName.isPresent()){
                    logger.info("The attribute " + entry.getValue() + " of class " + entry.getKey().getType().getCanonicalName() + " is not supported for the conversion and will not be saved");
                    continue;
                }
                if (entry.getValue().equals(Configuration.getConfiguration().ID_FIELD)){
                    document.addField(Configuration.getConfiguration().ID_FIELD,getSolrIdFromId(clazz, (String) entry.getKey().get(istance)));
                    continue;
                }
                //We reached this point, a valid and full field is here, can be a Solr special field or not
                if (SolrSpecialField.class.isAssignableFrom(fieldToSave.getType())){
                    SolrSpecialField field = (SolrSpecialField) fieldToSaveValue;
                    document.addField(solrFieldName.get(), field.getSolrFieldValue());
                    logger.debug("Serializing field of class " + field.getClass() + " as " + field.getSolrFieldValue());
                    continue;
                }
                //Handle the enum saving their value as plain String
                if (fieldToSave.getType().isEnum()){
                    document.addField(solrFieldName.get(), fieldToSaveValue.toString());
                    continue;
                }
                //Its a field that Solr should be able to handle
                document.addField(solrFieldName.get(), fieldToSaveValue);
            }
        } catch (IllegalAccessException e){
            logger.error("Exception accessing the field");
            logger.error(e.getMessage(), e);
            return Optional.absent();
        }
        return isValidDocument(document) ? Optional.of(document) : Optional.<SolrInputDocument>absent();
    }

    /**
     * @param document
     * @param <T>
     * @return an Optional containing the T class, will fail if trying to convert if try to convert class that are not subclass of BaseSolrResource
     */
    static <T extends BaseSolrIndexableResource> Optional<T> fromSolrDocument(SolrInputDocument document) {
        try {
            String className = (String) document.getFieldValue(Configuration.getConfiguration().CLASSNAME_SOLR_FIELD);
            String objectIdentifier = (String) document.getFieldValue(Configuration.getConfiguration().ID_FIELD);
            String id = getIdFromSolrId(objectIdentifier);
            if (className == null || objectIdentifier == null || id == null) throw new ClassNotSpecifiedException();
            T objectToFill = (T) Class.forName(className).getDeclaredConstructor(String.class).newInstance(id);
            //Already setted id and className now
            for(Map.Entry<String, SolrInputField> entry : document.entrySet()){
                Object solrFieldValue = entry.getValue().getValue();
                if (solrFieldValue == null) {
                    logger.info("ignoring null value for field in solr " + entry.getKey());
                    continue;
                }
                if (entry.getKey().equals(VERSION_SOLR_FIELD))
                    continue;
                //Id already setted
                if(entry.getKey().equals(Configuration.getConfiguration().ID_FIELD) ||
                        entry.getKey().equals(Configuration.getConfiguration().CLASSNAME_SOLR_FIELD)){
                    continue;
                }
                //We reach this, only attributes fields now, populate them
                String fieldName = getActualFieldNameFromSolrFieldName(entry.getKey());
                Field field = objectToFill.getClass().getField(fieldName);
                //If the field is a special field
                if (SolrSpecialField.class.isAssignableFrom(field.getType())){
                    //Be sure that the object has an empty constructor
                    //Let's create and istance of that and let it return a copy of the saved obj in solr
                    //Using the method in the interface
                    try {
                        solrFieldValue =
                                ((SolrSpecialField) field.getType().newInstance())
                                .fillFromSolrFieldValue(solrFieldValue);
                        //TODO return absent or simply a null field?
                    } catch (TransformationException e) {
                        logger.error(e);
                        continue;
                    }
                }
                //If the field is an enum we need to handle it retrieving the enum value from the string
                if (field.getType().isEnum()){
                    Class clz = field.getType();
                    solrFieldValue = Enum.valueOf(clz, (String) solrFieldValue);
                }
                field.set(objectToFill, solrFieldValue);
            }
            return Optional.of(objectToFill);
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage(), e);
        } catch (InstantiationException e) {
            logger.error("Check that the class has no empty constructor");
            logger.error(e.getMessage(),e);
        } catch (IllegalAccessException e) {
            logger.error("Tried to access to a field that does not exist or is private"+e.getMessage());
            logger.error(e.getMessage(),e);
        } catch (NoSuchFieldException e) {
            logger.error("Istances on solr and models are not aligned");
            logger.error(e.getMessage(),e);
        } catch (ClassNotSpecifiedException e) {
            logger.error(e.getMessage(),e);
        }  catch (NoSuchMethodException e) {
            logger.error(e.getMessage(),e);
        } catch (InvocationTargetException e) {
            logger.error(e.getMessage(),e);
            e.printStackTrace();
        }
        return Optional.absent();

    }

    static <T extends BaseSolrIndexableResource> Optional<T> fromSolrDocument(SolrDocument document) {
        SolrInputDocument simpleDocument = new SolrInputDocument();
        for(Map.Entry<String, Object> entry : document.entrySet()) {
            simpleDocument.addField(entry.getKey(), entry.getValue());
        }
        return fromSolrDocument(simpleDocument);
    }

    static <T extends BaseSolrIndexableResource> Collection<SolrInputDocument> toSolrDocumentCollection(Collection<T> istances){
        Collection<SolrInputDocument> docs = new LinkedList<>();
        for (T istance : istances){
            Optional<SolrInputDocument> d = toSolrDocument(istance);
            if (d.isPresent())
                docs.add(d.get());
        }
        return docs;
    }

    static String getSolrIdFromId(Class<? extends BaseSolrIndexableResource> aClass,final String id){
        return aClass.getCanonicalName()+ Configuration.getConfiguration().ID_FIELD_SEPARATOR+id.trim();
    }

    static String getIdFromSolrId(final String solrId) {
        int sepPosition =  solrId.indexOf(Configuration.getConfiguration().ID_FIELD_SEPARATOR);
        if (sepPosition < 0 || sepPosition >= solrId.length())
            return null;
        return solrId.substring(sepPosition+1,solrId.length());
    }
}
