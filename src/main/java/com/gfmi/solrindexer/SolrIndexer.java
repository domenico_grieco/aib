package com.gfmi.solrindexer;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.*;

import com.gfmi.solrindexer.config.Configuration;
import com.gfmi.solrindexer.exceptions.InvalidClassException;
import com.gfmi.solrindexer.models.BaseSolrIndexableResource;
import com.gfmi.solrindexer.utils.Tick;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.CloudSolrServer;
import org.apache.solr.client.solrj.impl.HttpClientUtil;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.client.solrj.response.FacetField;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.params.ModifiableSolrParams;

import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.google.common.collect.FluentIterable;
import com.google.common.collect.Lists;

/**
 * Class used to index beans on solr
 */
public class SolrIndexer {
    //Could be a simple installation on a solrCloud installation,
    //U choose based on the constructor you are using
    protected SolrServer solrClient;
    static final int defaultBatchCommit = 1000;

    static Logger logger = Logger.getLogger(SolrIndexer.class);

    /**
     * Used to connect to a simple solr installation, creating the HttpSolrClient
     * @param url the SIMPLE solr installation URL
     * @param collectionName
     */
    public SolrIndexer(final String url, final String collectionName) {
        String solrUrlWithCollection = url.charAt(url.length() - 1) == '/' ?
                url + collectionName + "/" :
                url + "/" +collectionName + "/";
        //TODO parametrize this stuff
        ModifiableSolrParams params = new ModifiableSolrParams();
        params.set("maxConnections", 256);
        params.set("maxConnectionsPerHost", 64);
        params.set("followRedirects", false);
        solrClient = new HttpSolrServer(solrUrlWithCollection, HttpClientUtil.createClient(params));
    }

    /**
     * Used to connect to a solrCloud given the set of zookeeper, the chRoot (Usualli /solr) and the name of the collection
     * @param zookeeperHosts The set of the zookeper hosts
     * @param chRoot The chroot
     * @param collectionName The collection name
     */
    public SolrIndexer(String zookeeperHosts, String chRoot, final String collectionName){
        String solrUrlWithCollection = chRoot.charAt(chRoot.length() - 1) == '/' ?
                chRoot + collectionName + "/" :
                chRoot + "/" +collectionName + "/";
        solrClient = new CloudSolrServer(zookeeperHosts);
    }

    /**
     * Used when we alreay have a client to solr and we want to use that
     * @param solrClient
     */
    public SolrIndexer(SolrServer solrClient){
        this.solrClient = solrClient;
    }

    protected SolrIndexer(){}
    /**
     * Check if the connection is ok
     * @return true if the connection is ok, false otherwise
     */
    public boolean checkConnection(){
        try {
            //A stupid select all limit 1 query
            SolrQuery simpleQuery = new SolrQuery("*:*");
            simpleQuery.setRows(1);
            solrClient.query(simpleQuery).getResults();
            return true;
        } catch (SolrServerException e) {
            logger.error(e.getMessage(), e);
            return false;
        } catch (Exception e ){
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    public <T extends BaseSolrIndexableResource> boolean uploadIstances(Collection<T> resources) {
        int batchCount = 1;
        try {
            for (T resource : resources) {
                SolrInputDocument document = SolrTransformer.toSolrDocument(resource).orNull();
                if (document != null)
                    solrClient.add(document);
                else
                    logger.warn("Faild to transform in SolrDocument " + resource);
                batchCount++;
                if (batchCount % defaultBatchCommit == 0) {
                    solrClient.commit();
                    logger.info("Batch committed");
                }
            }
            solrClient.commit();
            return true;
        } catch (SolrServerException e) {
            return false;
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            return false;
        }
    }

    public <T extends BaseSolrIndexableResource> boolean uploadIstancesUsingUpdateRequest(Collection<T> resources, int forkJoinPoolSize) {
        ForkJoinPool pool = new ForkJoinPool(forkJoinPoolSize);
        //TODO submit more tasks to the pool to make the request MT
        final BlockingQueue<T> resourceToUpload = new ArrayBlockingQueue<T>(resources.size());
        resourceToUpload.addAll(resources);
        try {
            pool.submit(() -> {
                String taskName = UUID.randomUUID().toString();
                logger.info("Hi! i'm task " + taskName);
                int batchCount = 1;
                UpdateRequest request = new UpdateRequest();
                try {
                    T resource;
                    while ((resource = resourceToUpload.poll(1, TimeUnit.SECONDS)) != null) {
                        SolrInputDocument document = SolrTransformer.toSolrDocument(resource).orNull();
                        if (document != null)
                            request.add(document);
                        else
                            logger.warn("Faild to transform in SolrDocument " + resource);
                        batchCount++;
                        if (batchCount % defaultBatchCommit == 0) {
                            request.process(getSolrClient());
                            request.clear();
                            logger.info("Batch processed on task named " + taskName);
                        }
                    }
                    request.process(getSolrClient());
                    request.clear();
                } catch (SolrServerException e) {
                    logger.error(e.getMessage(), e);
                } catch (IOException e) {
                    logger.error(e.getMessage(), e);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }).get();
            logger.info("Committing");
            Tick tick = new Tick();
            solrClient.commit();
            logger.info("Finished after " + tick.getTockInSeconds() + " seconds ");
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SolrServerException e) {
            e.printStackTrace();
        }
        return false;
    }

    public <T extends BaseSolrIndexableResource> List<T> retrieveIstances(SolrQueryOnBaseResource query, int limit)
            throws IOException, SolrServerException, ClassNotFoundException, IllegalAccessException, InstantiationException, InvalidClassException {
        query.setRows(limit);
        return retrieveIstances(query);
    }

    public <T extends BaseSolrIndexableResource> List<T> retrieveIstances(SolrQueryOnBaseResource query, int page, int pageSize) throws IllegalAccessException, InvalidClassException, IOException, InstantiationException, ClassNotFoundException, SolrServerException {
        query.setStart((page - 1) * pageSize);
        query.setRows(pageSize);
        return retrieveIstances(query);
    }

    private <T extends BaseSolrIndexableResource> List<T> retrieveIstances(SolrQueryOnBaseResource query)
            throws IOException, SolrServerException, ClassNotFoundException, IllegalAccessException, InstantiationException, InvalidClassException {
        return retrieveIstances((SolrQuery) query);
    }


    protected  <T extends BaseSolrIndexableResource> List<T> retrieveIstances(SolrQuery query)
            throws IOException, SolrServerException, ClassNotFoundException, IllegalAccessException, InstantiationException, InvalidClassException {
        logger.info("Making query :" + query.getQuery());
        Tick tick = new Tick();
        QueryResponse result = solrClient.query(query);
        SolrDocumentList documentList = result.getResults();
        logger.debug("Query executed in " + tick.getTock() + " millis, fetcted istances number : " + documentList.size());
        logger.debug("Trasforming into equivalent classes");
        tick = new Tick();
        List<T> results = new LinkedList<>();
        for (SolrDocument  d : documentList) {
            Optional<T> istance = SolrTransformer.fromSolrDocument(d);
            if (istance.isPresent())
                results.add(istance.get());
            else
                logger.warn(String.format("Error building class instance from Solr document %s",d.toString()));
        }
        logger.debug("Trasformation done in millis" + tick.getTock());
        return results;
    }

    public List<SolrDocument> retrieveIstancesAsSolrDocument(SolrQuery query)
            throws IOException, SolrServerException, ClassNotFoundException, IllegalAccessException, InstantiationException, InvalidClassException {
        logger.info("Making query :" + query.getQuery());
        QueryResponse result = solrClient.query(query);
        SolrDocumentList documentList = result.getResults();
        List<SolrDocument> results = new LinkedList<>();
        for (SolrDocument  d : documentList) {
            results.add(d);
        }
        return results;
    }

    public <T extends BaseSolrIndexableResource> List<T> retrieveAllIstances(SolrQueryOnBaseResource query) throws SolrServerException, IOException, IllegalAccessException, InstantiationException, ClassNotFoundException, InvalidClassException {
        return retrieveIstances(query,getMaxResults(query));
    }

    public <T extends BaseSolrIndexableResource> List<T> retrieveAllIstances(SolrQueryBuilder query) throws SolrServerException, IOException, IllegalAccessException, InstantiationException, ClassNotFoundException, InvalidClassException {
        SolrQueryOnBaseResource q = new SolrQueryOnBaseResource(query);
        return retrieveIstances(q,getMaxResults(q));
    }


    public <T extends BaseSolrIndexableResource> void deleteById(Class<T> aClass, String id) throws IOException, SolrServerException {
        deleteByIds(aClass, Lists.newArrayList(id));
    }


    public <T extends BaseSolrIndexableResource> void deleteByIds(final Class<T> aClass, final Collection<String> id) throws IOException, SolrServerException {
        List<String> realIdList = FluentIterable.from(id).transform(new Function<String, String>() {
            @Override
            public String apply(String s) {
                return SolrTransformer.getSolrIdFromId(aClass,s);
            }
        }).toList();
        solrClient.deleteById(realIdList);
        solrClient.commit();
    }

    public <T extends BaseSolrIndexableResource> void deleteByQuery(SolrQueryOnBaseResource query) throws IOException, SolrServerException {
        solrClient.deleteByQuery(query.getQuery());
        solrClient.commit();
    }

    public void deleteByQuery(String query) throws IOException, SolrServerException {
        solrClient.deleteByQuery(query);
        solrClient.commit();
    }

    public Map<String, Long> retrieveAllTheClassNameInCollection() throws SolrServerException {
        Map<String, Long> fullClassNameMap = new LinkedHashMap<>();
        SolrQuery query = new SolrQuery("*:*");
        query.addFacetField(Configuration.getConfiguration().CLASSNAME_SOLR_FIELD);
        query.setRows(1);
        QueryResponse result = solrClient.query(query);
        FacetField classNameFacet = result.getFacetField(Configuration.getConfiguration().CLASSNAME_SOLR_FIELD);
        for (FacetField.Count c : classNameFacet.getValues()){
            fullClassNameMap.put(c.getName(), c.getCount());
        }
        return fullClassNameMap;
    }

    private int getMaxResults(SolrQuery query) throws SolrServerException {
        query.setRows(1);
        QueryResponse result = solrClient.query(query);
        return (int) result.getResults().getNumFound();
    }

    public SolrServer getSolrClient(){
        return solrClient;
    }

}