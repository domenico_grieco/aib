package com.gfmi.solrindexer.interfaces;

import com.gfmi.solrindexer.exceptions.TransformationException;

/**
 * Created by fre on 14/10/15.
 */
public interface SolrSpecialField {
    /**
     * @return the object we wish to save in the Solr Document
     */
    Object getSolrFieldValue();

    /**
     * Take the object that comes from Solr and return the actual object
     * @param valueThatComesFromSolr
     * @throws TransformationException
     */
    SolrSpecialField fillFromSolrFieldValue(Object valueThatComesFromSolr) throws TransformationException;

    /**
     * @return the postfix that is used to target the type in solr using dynamic field feature
     */
    String getDynamicFieldPostfix();
}
