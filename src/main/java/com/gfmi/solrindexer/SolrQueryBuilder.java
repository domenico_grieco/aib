package com.gfmi.solrindexer;

import com.gfmi.solrindexer.exceptions.InvalidFieldException;
import com.gfmi.solrindexer.models.BaseSolrIndexableResource;
import com.google.common.base.Optional;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by fre on 22/09/15.
 * Class used to make query on the class fields
 */
public class SolrQueryBuilder extends NaiveSolrQueryBuilder{
    Logger logger = Logger.getLogger(SolrQueryBuilder.class);
    private Class<? extends BaseSolrIndexableResource> aClass;

    public SolrQueryBuilder(Class<? extends BaseSolrIndexableResource> aClass) {
        super(aClass.getCanonicalName());
        this.aClass = aClass;
    }

    public SolrQueryBuilder(Class<? extends BaseSolrIndexableResource> aClass, boolean useFullCanonical) {
        super(aClass.getCanonicalName(), aClass.getSimpleName(), useFullCanonical);
        this.aClass = aClass;
    }

    public SolrQueryBuilder addConstraint(final String fieldName,final String constraint) throws NoSuchFieldException {
        Field field = aClass.getField(fieldName);

        if (field.getName().equals(configuration.ID_FIELD)){
            this.constraintMap.put(configuration.ID_FIELD, SolrTransformer.getSolrIdFromId(aClass, constraint));
            return this;
        }
        if (field == null || constraint == null){
            logger.error(String.format("Tried to add an invalid field constraint couple :%s - %s\n One or both are null", field, constraint));
            return this;
        }
        if (!isHandledByTrasformer(field)){
            logger.error(String.format("Don't know how to name in solr the field %s",field.getName()));
            return this;
        }
        Optional<String> fieldNameInSolr = ClassUtils.getFieldNameInSolr(field);
        this.constraintMap.put(fieldNameInSolr.get(), constraint);
        return this;
    }

    public SolrQueryBuilder removeConstraint(final String fieldName){
        Field field;
        try {
            field = aClass.getField(fieldName);
         Optional<String> fieldNameInSolr = ClassUtils.getFieldNameInSolr(field);
        if (!isHandledByTrasformer(field) || !fieldNameInSolr.isPresent()){
            logger.error(String.format("Don't know how to name in solr the field %s, can't put nor remove a constraint on that",field.getName()));
            return this;
        }
        if (constraintMap.containsKey(fieldNameInSolr.get()))
            constraintMap.remove(fieldNameInSolr.get());
        else
            logger.warn("No constraint on " + fieldName);
        return this;
        } catch (NoSuchFieldException e) {
            logger.warn(String.format("tried to remove a constraint on field %s but no field like that exist", fieldName));
            return this;
        }
    }

    public SolrQueryBuilder addConstraintOrFail(final String fieldName, final String constraint) throws InvalidFieldException {
        Field field = null;
        try {
            field = aClass.getField(fieldName);
        } catch (Exception e) {}
        if (field == null || constraint == null || constraint.isEmpty()){
            throw new InvalidFieldException(String.format("Tried to add an invalid field constraint couple :%s - %s\n One or both are null/empty or does not exist", field, constraint));
        }
        if (!isHandledByTrasformer(field)){
            throw new InvalidFieldException(String.format("Don't know how to name in solr the field %s",field.getName()));
        }
        if (field.getName().equals(configuration.ID_FIELD)){
            this.constraintMap.put(configuration.ID_FIELD, SolrTransformer.getSolrIdFromId(aClass, constraint));
            return this;
        } else {
            Optional<String> fieldNameInSolr = ClassUtils.getFieldNameInSolr(field);
            this.constraintMap.put(fieldNameInSolr.get(), constraint);
        }
        return this;
    }

    public String getStringQuery(){
        String query = "";
        Iterator<Map.Entry<String,String>> iterator = constraintMap.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, String> next = iterator.next();
            query+=String.format("(%s:%s)",next.getKey(),next.getValue());
            if (iterator.hasNext())
                query+=" AND ";
        }
        return query;
    }

    public SolrQueryOnBaseResource toSolrQueryOnBaseResource(){
        return new SolrQueryOnBaseResource(this);
    }

    public SolrQuery getSolrQuery(){
        return new SolrQuery(getStringQuery());
    }

    public SolrQueryOnBaseResource toSolrQuery() { return new SolrQueryOnBaseResource(this); }

    /**
     * Check if the transformer know how to handle this field
     * @param field
     * @return
     */
    private boolean isHandledByTrasformer(Field field){
        return ClassUtils.getFieldNameInSolr(field).isPresent();
    }

    /**
     *
     * @return
     */
    public Class<? extends BaseSolrIndexableResource> getQueryClass(){
        return aClass;
    }
}
