package com.gfmi.solrindexer;

import com.gfmi.solrindexer.annotations.CustomDynamicField;
import com.gfmi.solrindexer.config.Configuration;
import com.gfmi.solrindexer.interfaces.SolrSpecialField;
import com.gfmi.solrindexer.models.BaseSolrIndexableResource;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableMap;
import org.apache.log4j.Logger;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Created by fre on 18/03/15.
 */
public class ClassUtils {

    /**
     * Maps primitive {@code Class}es to their corresponding wrapper {@code Class}.
     */
    public static final Map<Class<?>, Class<?>> primitiveWrapperMap = ImmutableMap.<Class<?>, Class<?>>builder()
            .put(Long.TYPE, Long.class)
            .put(Boolean.TYPE, Boolean.class)
            .put(Short.TYPE, Short.class)
            .put(Byte.TYPE, Byte.class)
            .put(Integer.TYPE, Integer.class)
            .put(Character.TYPE, Character.class)
            .put(Double.TYPE, Double.class)
            .put(Float.TYPE, Float.class)
            .put(Void.TYPE, Void.TYPE)
            .build();

    static final Logger logger = Logger.getLogger(ClassUtils.class);
    /**
     * Convert a primitive class to his wrapper class
     * @param cls
     * @return
     */
    public static Class<?> primitiveToWrapper(final Class<?> cls) {
        Class<?> convertedClass = cls;
        if (cls != null && cls.isPrimitive()) {
            convertedClass = primitiveWrapperMap.get(cls);
        }
        return convertedClass;
    }

    /**
     * Given a field, return what is the name of the field in solr, if the field can be serializable on Solr, null othewise
     * null otherwise
     * @param field
     * @return
     */
    public static Optional<String> getFieldNameInSolr(final Field field)  {
        if (SolrSpecialField.class.isAssignableFrom(field.getType())){
            try {
                Class fieldType =  field.getType();
                SolrSpecialField specialField = (SolrSpecialField) fieldType.newInstance();
                return Optional.of(field.getName()+specialField.getDynamicFieldPostfix());
            } catch (InstantiationException | IllegalAccessException e) {
                logger.error(String.format("Tried to create an istance of class %s to get his suffix, but got exception.\n" +
                        "Be sure that class has en empty public constructor"
                        ,field.getClass().getCanonicalName()),e);
                return Optional.absent();
            }
        }
        String solrPostFix = null;
        if (field.getType().equals(List.class)) {
            final Type t = field.getGenericType();
            if (t instanceof ParameterizedType) {
                Class<?> innerClassInList = (Class<?>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                solrPostFix = getSolrPostFixForClass(innerClassInList,field.getDeclaredAnnotations());
                if (solrPostFix == null) {
                    logger.error(String.format("Don't know how to treat class %s inside the collection in field %s", innerClassInList, field));
                    return Optional.absent();
                } else
                    solrPostFix = solrPostFix+ Configuration.getConfiguration().MULTIVALUE_POSTFIX;
            }
        } else
            solrPostFix = getSolrPostFixForClass(field.getType(), field.getDeclaredAnnotations());
        if (solrPostFix == null)
            return Optional.absent();
        return Optional.of(field.getName()+solrPostFix);
    }

    /**
     *
     * @param fieldClass the class of the Field we want to calculate the postfix
     * @param annotations the list of annotations applied to the field
     * @return
     */
    private static String getSolrPostFixForClass(Class fieldClass, Annotation[] annotations){
        if (!Configuration.getConfiguration().CLASS_FIELD_MAP.containsKey(fieldClass))
            fieldClass = primitiveToWrapper(fieldClass);
        //Special case for enum
        if (fieldClass.isEnum()){ //Save the enum with the same postfix used for string
            return Configuration.getConfiguration().CLASS_FIELD_MAP.get(String.class);
        }
        String classSolrIdentifier = Configuration.getConfiguration().CLASS_FIELD_MAP.get(fieldClass);
        if (classSolrIdentifier == null) // We don't know how to treat this kind of field
            return null;
        //If is annotated with a customDynamicField annotation,
        //we must fetch the value from annotation and use that as postfix value
        for (Annotation annotation : annotations){
            if (annotation.annotationType().equals(CustomDynamicField.class)){
                classSolrIdentifier = ((CustomDynamicField) annotation).dynamicFieldPostfix();
            }
        }
        return classSolrIdentifier;
    }

    /**
     * Return what will be the name of the field in solr, null if it's invalid or field does not exist
     * @param aClass
     * @param fieldName
     * @return
     */
    public static Optional<String> getFieldNameInSolr(Class<? extends BaseSolrIndexableResource> aClass, final String fieldName) {
        Map<Field,String> fields = getAllSerializableFields(aClass);
        for (Field f : fields.keySet()){
            if (f.getName().equals(fieldName))
                return getFieldNameInSolr(f);
        }
        return Optional.absent();
    }

    /**
     * Return all the public non static fields of a given class in a Field Name - Field map
     * @param aClass
     * @return
     */
    public static Map<Field, String> getAllSerializableFields(Class<?> aClass){
        Map<Field,String> fieldMap = new HashMap<>(aClass.getFields().length);
        for (Field f : aClass.getFields()){
            if (!(java.lang.reflect.Modifier.isStatic(f.getModifiers()))){
                fieldMap.put(f,f.getName());
            }
        }
        return ImmutableMap.copyOf(fieldMap);
    }
}
