package com.gfmi.solrindexer.runnable;


import com.gfmi.solrindexer.SolrQueryOnBaseResource;
import com.gfmi.solrindexer.models.BaseSolrIndexableResource;
import com.gfmi.solrindexer.SolrIndexer;
import com.gfmi.solrindexer.SolrQueryBuilder;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrDocumentList;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
/**
 * Created by fre on 21/10/15.
 */
public class ExecuteCommand {
    /**
     * Main used to execute some commands
     * Main is expecting the following parameters:
     //Solr url
     //Solr collection
     //Class we wish to select or delete
     //Command to be executed - Select or Delete are the only possible values
     //Query to be executed
     * @param args
     */
    public static void main(String[] args){
        if (args.length != 5){
            System.out.println("Wrong number parameters: Usage SolrUrl Solrcollection Class Command (select or delete) query");
            System.exit(1);
        }

        String url = args[0];           //TODO VALIDATE ?
        String collectionName = args[1];//TODO VALIDATE ?
        String fullClassName = args[2]; //Validated
        String command = args[3];       //Validated
        String queryArgument = args[4];         //Cannot be validated easily"

        Object istanceOfClass = null;
        Class< ? extends BaseSolrIndexableResource> istanceOfClassValidated = null;
        try {
            istanceOfClass = Class.forName(fullClassName).newInstance();
            if (!(istanceOfClass instanceof BaseSolrIndexableResource))
                throw new Exception("Class not istance of BaseSolrIndeaxableResource");
            else
                istanceOfClassValidated = (Class<? extends BaseSolrIndexableResource>) istanceOfClass.getClass();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            istanceOfClass = null;
        }

        //Validating class
        if (istanceOfClass == null)
            System.exit(1);

        //Validating command
        if (!((command.equalsIgnoreCase("select") || command.equalsIgnoreCase("delete")))){
            System.out.println("Wrong command, only select or delete are allowed");
            System.exit(1);
        }


        try {
            SolrIndexer indexer = new SolrIndexer(url,collectionName);
            SolrQueryOnBaseResource solrQuery = new SolrQueryOnBaseResource(new SolrQueryBuilder(istanceOfClassValidated));
            String partialQueryString = solrQuery.getQuery();
            String fullQueryString = partialQueryString;
            if (!queryArgument.isEmpty())
                fullQueryString = partialQueryString + " AND " + queryArgument;
            System.out.println("Full query on solr will be :\n" + fullQueryString);
            System.out.println("Command will be " + command);
            if (command.equalsIgnoreCase("delete")){
                System.out.println("Are you sure? Yy/Nn");{
                    String answer = new Scanner(System.in).nextLine();
                    if (!answer.equalsIgnoreCase("y")){
                        System.out.printf("Exiting");
                        System.exit(1);
                    }
                }
                indexer.getSolrClient().deleteByQuery(fullQueryString);
                indexer.getSolrClient().commit();
                System.out.println("Executed and succesfully committed");
            } else{
                SolrQuery solrQuerySelect = new SolrQuery(fullQueryString).setRows(Integer.MAX_VALUE);
                SolrDocumentList docList = indexer.getSolrClient().query(solrQuerySelect).getResults();
                System.out.println("Retrieved docs length " + docList.size());
                JSONObject returnResults = new JSONObject();
                Map<Integer, Object> solrDocMap = new HashMap<Integer, Object>();
                int counter = 1;
                for(Map singleDoc : docList)
                {
                    solrDocMap.put(counter, new JSONObject(singleDoc));
                    counter++;
                }
                returnResults.put("docs", solrDocMap);
                System.out.println(returnResults.toString());
            }
        } catch (SolrServerException | IOException e) {
            System.out.println("Failed to delete");
            e.printStackTrace();
            System.exit(1);
        } catch (JSONException e) {
            System.out.println("Failed to jsonize stuff");
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("Succesfully ended, exiting");
        System.exit(0);
    }


    public static void dropCollection(String url, String collectionName, String fullClassName, String command, String queryArgument){
        /*
        String url = args[0];           //TODO VALIDATE ?
        String collectionName = args[1];//TODO VALIDATE ?
        String fullClassName = args[2]; //Validated
        String command = args[3];       //Validated
        String queryArgument = args[4];         //Cannot be validated easily"
        */

        Object istanceOfClass = null;
        Class< ? extends BaseSolrIndexableResource> istanceOfClassValidated = null;
        try {
            istanceOfClass = Class.forName(fullClassName).newInstance();
            if (!(istanceOfClass instanceof BaseSolrIndexableResource))
                throw new Exception("Class not istance of BaseSolrIndeaxableResource");
            else
                istanceOfClassValidated = (Class<? extends BaseSolrIndexableResource>) istanceOfClass.getClass();
        } catch (Exception e) {
            System.out.println(e.getMessage());
            istanceOfClass = null;
        }

        //Validating class
        if (istanceOfClass == null)
            System.exit(1);

        //Validating command
        if (!((command.equalsIgnoreCase("select") || command.equalsIgnoreCase("delete")))){
            System.out.println("Wrong command, only select or delete are allowed");
            System.exit(1);
        }


        try {
            SolrIndexer indexer = new SolrIndexer(url,collectionName);
            SolrQueryOnBaseResource solrQuery = new SolrQueryOnBaseResource(new SolrQueryBuilder(istanceOfClassValidated));
            String partialQueryString = solrQuery.getQuery();
            String fullQueryString = partialQueryString;
            if (!queryArgument.isEmpty())
                fullQueryString = partialQueryString + " AND " + queryArgument;
            System.out.println("Full query on solr will be :\n" + fullQueryString);
            System.out.println("Command will be " + command);
            if (command.equalsIgnoreCase("delete")){
                System.out.println("Are you sure? Yy/Nn");{
                    String answer = new Scanner(System.in).nextLine();
                    if (!answer.equalsIgnoreCase("y")){
                        System.out.printf("Exiting");
                        System.exit(1);
                    }
                }
                indexer.getSolrClient().deleteByQuery(fullQueryString);
                indexer.getSolrClient().commit();
                System.out.println("Executed and succesfully committed");
            } else{
                SolrQuery solrQuerySelect = new SolrQuery(fullQueryString).setRows(Integer.MAX_VALUE);
                SolrDocumentList docList = indexer.getSolrClient().query(solrQuerySelect).getResults();
                System.out.println("Retrieved docs length " + docList.size());
                JSONObject returnResults = new JSONObject();
                Map<Integer, Object> solrDocMap = new HashMap<Integer, Object>();
                int counter = 1;
                for(Map singleDoc : docList)
                {
                    solrDocMap.put(counter, new JSONObject(singleDoc));
                    counter++;
                }
                returnResults.put("docs", solrDocMap);
                System.out.println(returnResults.toString());
            }
        } catch (SolrServerException | IOException e) {
            System.out.println("Failed to delete");
            e.printStackTrace();
            System.exit(1);
        } catch (JSONException e) {
            System.out.println("Failed to jsonize stuff");
            e.printStackTrace();
            System.exit(1);
        }
        System.out.println("Succesfully ended, exiting");
        System.exit(0);
    }

}
