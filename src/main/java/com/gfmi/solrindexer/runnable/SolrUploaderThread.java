package com.gfmi.solrindexer.runnable;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.request.UpdateRequest;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by fre on 26/07/16.
 */
public class SolrUploaderThread implements Runnable{
    private String threadName = UUID.randomUUID().toString();
    private Logger logger = Logger.getLogger(this.getClass().getName()+"-"+threadName);
    private HttpSolrServer solrServer;
    private BlockingQueue<SolrInputDocument> documents;
    private int requestSize = 1000;

    public SolrUploaderThread(HttpSolrServer solrServer, BlockingQueue<SolrInputDocument> documentsToUploadQueue){
        this.solrServer = solrServer;
        this.documents = documentsToUploadQueue;
    }

    public SolrUploaderThread(HttpSolrServer solrServer, BlockingQueue<SolrInputDocument> documentsToUploadQueue, int requestSize){
        this.solrServer = solrServer;
        this.documents = documentsToUploadQueue;
        this.requestSize = requestSize;
    }

    @Override
    public void run() {
            logger.info("Hi from thread " + threadName);
            int batchCount = 1;
            UpdateRequest request = new UpdateRequest();
            try {
                SolrInputDocument dooToUpload ;
                while ((dooToUpload = documents.poll(5, TimeUnit.SECONDS)) != null) {
                    request.add(dooToUpload);
                    batchCount++;
                    if (batchCount % requestSize == 0) {
                        request.process(solrServer);
                        request.clear();
                        logger.debug("Batch processed");
                    }
                    if ((batchCount % requestSize*10) == 0) //Log to info every 10 * batchsize
                        logger.info("Processed docs : " + requestSize*10);
                }
                request.process(solrServer);
                request.clear();
            } catch (SolrServerException e) {
                logger.error(e.getMessage(), e);
            } catch (IOException e) {
                logger.error(e.getMessage(), e);
            } catch (InterruptedException e) {
                logger.error(e.getMessage(), e);
            }
    }
}