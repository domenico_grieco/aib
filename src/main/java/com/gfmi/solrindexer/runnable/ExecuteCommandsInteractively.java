package com.gfmi.solrindexer.runnable;

import com.google.common.collect.ImmutableMap;
import com.gfmi.solrindexer.NaiveSolrQueryBuilder;
import com.gfmi.solrindexer.SolrIndexer;
import com.gfmi.solrindexer.exceptions.InvalidClassException;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.common.SolrDocument;

import java.io.IOException;
import java.util.*;

/**
 * Created by fre on 10/11/15.
 */
public class ExecuteCommandsInteractively {

    static Scanner scanner = new Scanner(System.in);

    static enum Commands{
        SELECT,
        DELETE
    }

    public static void main(String[] args) throws SolrServerException, IOException, ClassNotFoundException, InvalidClassException, InstantiationException, IllegalAccessException {
        SolrIndexer indexer = null;
        Map<Integer, Commands> commandChosenMap = ImmutableMap.of(0,Commands.DELETE,1,Commands.SELECT);
        int classCount = 0;
        while (indexer == null)
            indexer = getIndexer();
        System.out.println("Indexer built correctly");
        do {
            Map<Integer, String> fullClassNameChoiceMap = new HashMap<>();
            for (Map.Entry<String, Long> entry : indexer.retrieveAllTheClassNameInCollection().entrySet()) {
                fullClassNameChoiceMap.put(classCount, entry.getKey());
                classCount++;
            }
            System.out.printf("Got this classes\n");
            String fullClassName = choose(fullClassNameChoiceMap);
            Commands commandChosen = choose(commandChosenMap);
            NaiveSolrQueryBuilder naiveSolrQueryBuilder = new NaiveSolrQueryBuilder(fullClassName);
            String fullQuery = naiveSolrQueryBuilder.getStringQuery();
            String queryArgument = getQueryAdditionalArgument();
            if (!queryArgument.trim().isEmpty()) {
                fullQuery += " AND " + queryArgument;
            }
            if (askYes(commandChosen + " on this query will be performed\n" + fullQuery+"\nContinue?")) {
                if (commandChosen == Commands.DELETE) {
                    indexer.deleteByQuery(fullQuery);
                    System.out.println("Performed and committed");
                } else {
                    List<SolrDocument> list = indexer.retrieveIstancesAsSolrDocument(new SolrQuery(fullQuery));
                    if (askYes(("Retrieved a list of size " + list.size() + " wanna see it? "))) {
                        for (SolrDocument d : list) {
                            System.out.println(d);
                        }
                    }
                }
            }
        }while (askYes("I'm done, whish to do something else"));
        System.out.printf("Bye");
        System.exit(0);
    }

    public static SolrIndexer getIndexer(){
        System.out.println("Insert full server url (Ex. http://localhost:8983/solr):");
        String url = scanner.nextLine().trim();
        System.out.println("Full server url: "+url);
        System.out.println("Insert collection name :");
        String collectionName = scanner.nextLine().trim();
        System.out.println("Collection name: "+url);
        SolrIndexer solrIndexer = new SolrIndexer(url,collectionName);
        return solrIndexer.checkConnection() ? solrIndexer : null;
    }

    public static Commands getCommand(){
        System.out.println("What do you wish to do?\n1) SELECT \n2) DELETE");
        try {
            int choice = Integer.parseInt(scanner.nextLine().trim());
            if (choice == 1)
                return Commands.SELECT;
            return Commands.DELETE;
        }catch (NumberFormatException e){
            System.out.printf("Not a number");
            return null;
        }
    }

    public static <T> T choose(Map<Integer,T> choiceMap){
        Integer number;
        do {
            for (Map.Entry<Integer, ? extends Object> choices : choiceMap.entrySet()){
                System.out.println(choices.getKey() + ") - " + choices.getValue().toString());
            }
            System.out.println("Enter your choice");
            try {
                number = Integer.parseInt(scanner.nextLine().trim());
                if (!choiceMap.keySet().contains(number)){
                    System.out.println("Wrong choice");
                    number = null;
                }
            }catch (NumberFormatException e){
                number =null;
            }
        }while (number == null);
        return choiceMap.get(number);
    }

    public static String getQueryAdditionalArgument(){
        System.out.println("Insert the query predicate or none");
        return scanner.nextLine();
    }

    public static boolean askYes(String message){
        System.out.println(message+"?Yy/Nn");
        return scanner.nextLine().trim().equalsIgnoreCase("y");
    }

}
