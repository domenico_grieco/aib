package com.gfmi.solrindexer.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Fre on 07/10/15.
 * Custom dynamic field annotation can be used when we have a
 * primitive or primitive wrapper class, and we wish to apply them a different postfix
 * for the name in order to be treated differently, accordingly to our schema.xml specification
 *
 * For example all the string are treated by default as solr type string.
 * This means that they are not tokenized and the research are case sensitive.
 * Suppose we have another type in solr Text General that perform some preprocessing on the string
 * (such as stemming, stopwords removal and so on) and that is targeted in our schema.xml by the postfix _t
 * (<dynamicField name="*_t"  type="text_general"    indexed="true"  stored="true"/>)
 * We wish that one string attribute (description for example) has to be treated as Text General by solr, so
 * we simply put the CustomDynamicField on the field with dynamixFielPostfix equal to _t.
 * Doing so the field description will be saved in a Solr document with the value description_t and hence,
 * treated as a Text General field.
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CustomDynamicField {
    String dynamicFieldPostfix();
}