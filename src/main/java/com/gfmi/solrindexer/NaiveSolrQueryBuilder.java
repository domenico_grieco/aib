package com.gfmi.solrindexer;

import com.gfmi.solrindexer.config.Configuration;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by fre on 10/02/16.
 * Naive SolrQueryBuilder that will no check what is given in input
 * and simply convert it as a query to perform on solr, without checking on class or on fields
 */
public class NaiveSolrQueryBuilder {
    protected String fullClassName;
    protected Configuration configuration;
    protected Map<String,String> constraintMap;


    public NaiveSolrQueryBuilder(String fullClassName){
        this.fullClassName = fullClassName;
        this.configuration = Configuration.getConfiguration();
        this.constraintMap = new LinkedHashMap<>();
        this.constraintMap.put(configuration.CLASSNAME_SOLR_FIELD, fullClassName);
    }

    public NaiveSolrQueryBuilder(String fullClassName, String simpleClassName, boolean useFullCanonicalName){
        this.fullClassName = fullClassName;
        this.configuration = Configuration.getConfiguration();
        this.constraintMap = new LinkedHashMap<>();
        if (useFullCanonicalName)
            this.constraintMap.put(configuration.CLASSNAME_SOLR_FIELD, fullClassName);
        else
            this.constraintMap.put(configuration.CLASSNAME_FIELD_NAME, "*."+simpleClassName);
    }


    public NaiveSolrQueryBuilder addNaiveConstraint(final String fieldNameInSolr,final String constraintInSolr) throws NoSuchFieldException {
        this.constraintMap.put(fieldNameInSolr, constraintInSolr);
        return this;
    }

    public String getStringQuery(){
        String query = "";
        Iterator<Map.Entry<String,String>> iterator = constraintMap.entrySet().iterator();
        while(iterator.hasNext()){
            Map.Entry<String, String> next = iterator.next();
            query+=String.format("(%s:%s)",next.getKey(),next.getValue());
            if (iterator.hasNext())
                query+=" AND ";
        }
        return query;
    }

}
