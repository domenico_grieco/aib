package com.gfmi.aib.solr;

/**
 * Created by biadmin on 9/20/16.
 */

import com.gfmi.solrindexer.models.BaseSolrIndexableResource;

/**
 * Created by biadmin on 2/24/16.
 */
public class SolrRule extends BaseSolrIndexableResource {

    public final static String ID_SEP = "-";
    public String type;
    public String hostname;
    public String conf;

    public static final String RULE_TYPE = "type";
    public static final String RULE_HOSTNAME = "hostname";
    public static final String RULE_CONF = "conf";


    public SolrRule(String id) {
        super(id);
    }

    public SolrRule(String type, String hostname, String conf) {
        super(type + ID_SEP + hostname);
        this.type = type;
        this.hostname = hostname;
        this.conf = conf;
    }

    @Override
    public String toString() {
        return "SolrRule{" +
                "type='" + type + '\'' +
                ", hostname='" + hostname + '\'' +
                ", conf='" + conf + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        SolrRule solrRule = (SolrRule) o;

        if (type != null ? !type.equals(solrRule.type) : solrRule.type != null) return false;
        return hostname != null ? hostname.equals(solrRule.hostname) : solrRule.hostname == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (hostname != null ? hostname.hashCode() : 0);
        return result;
    }
}
