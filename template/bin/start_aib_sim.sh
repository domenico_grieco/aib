#!/usr/bin/env bash

#-----------------------------------------------------------------------#
#                                                                       #
# Job Arguments                                                         #
#                                                                       #
#-----------------------------------------------------------------------#

# CONFIGURATION FILE
CFG_FILE="../cfg/spark-streming.conf"

#-----------------------------------------------------------------------#
#                                                                       #
# Spark Configuration                                                   #
#                                                                       #
#-----------------------------------------------------------------------#
NUM_EXECUTORS=1
EXECUTOR_MEMORY=1g
EXECUTOR_CORES=1
DRIVER_MEMORY=1g
#MASTER=yarn-cluster
MASTER=local[*]

GPARENT=`dirname "$PWD"`
NAME=`basename $GPARENT`

APP_NAME=$NAME-aibtest

#-----------------------------------------------------------------------#
#                                                                       #
# Job Configuration                                                     #
# (unmodifiable)                                                        #
#                                                                       #
#-----------------------------------------------------------------------#

jars=
SEP=,
MAIN_LIB_DIR=../lib

# MAIN LIB
for f in $MAIN_LIB_DIR/*.jar; do jars=$jars$SEP$f; done

# remove last separator ;
jars=${jars:1}

MAIN_CLASS=com.gfmi.aib.app.SparkStramingApp
MAIN_JAR=../lib/main/aib-1.0-SNAPSHOT.jar

spark-submit --name $APP_NAME --master $MASTER  --num-executors $NUM_EXECUTORS \
    --driver-memory $DRIVER_MEMORY \
    --executor-memory $EXECUTOR_MEMORY \
    --executor-cores $EXECUTOR_CORES \
    --class $MAIN_CLASS --jars $jars $MAIN_JAR \
    "$(cat $CFG_FILE)"
