#!/usr/bin/env bash
#                                                                                   

SCRIPT_NAME=$0
GPARENT=`dirname "$PWD"`
NAME=`basename $GPARENT`


#-----------------------------------------------------------------------------------#
#                                                                                   #
# Application configuration                                                         #
# (unmodifiable)                                                                    #
#-----------------------------------------------------------------------------------#

SCRIPT_LOG_FILE=../log/putKafka.out
PID_FILE=putKafka.pid

echo $SCRIPT_NAME
echo $GPARENT

LOG4J_CONF=../cfg/log4j-console.properties
CLASSPATH=../lib/*:../lib/main/*
CLASS_NAME=com.gfmi.aib.app.PutKafkaApp

CFG_FILE=../cfg/put-kafka.conf

java -cp "$CLASSPATH" -Dlog4j.configuration=$LOG4J_CONF $CLASS_NAME $CFG_FILE
