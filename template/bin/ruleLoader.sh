#!/usr/bin/env bash
#                                                                                   

SCRIPT_NAME=$0
GPARENT=`dirname "$PWD"`
NAME=`basename $GPARENT`


#-----------------------------------------------------------------------------------#
#                                                                                   #
# Application configuration                                                         #
# (unmodifiable)                                                                    #
#-----------------------------------------------------------------------------------#

SCRIPT_LOG_FILE=../log/ruleLoader.out
PID_FILE=ruleLoader.pid

echo $SCRIPT_NAME
echo $GPARENT

LOG4J_CONF=../cfg/log4j-console.properties
CLASSPATH=../lib/*:../lib/main/*
CLASS_NAME=com.gfmi.aib.app.RulesLoaderApp


if [ "$#" -ne 2 ]; then
    echo "Usage $0 <cfg-file> <rule-file>"
    exit 1
fi


CFG_FILE=$1
RULES_FILE=$2

echo "Loading rules..."
java -cp "$CLASSPATH" -Dlog4j.configuration=$LOG4J_CONF $CLASS_NAME $CFG_FILE $RULES_FILE 

echo "Done."
